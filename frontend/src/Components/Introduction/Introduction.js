import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const Introduction = ({setStage}) => {
    const handleClick = () => {
        setStage('SelectLocationType');
    };

    return (
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 reverse-mobile">
                        <div class="floating-img right"><a href="en/references/csg-ts/"><img
                                    src="fileadmin/user_upload/content/it/csgts_01_400x300.jpg" title="" alt=""
                                    class="" /></a>
                            <div class="floating-img-content ">
                                <h4>CSG-TS</h4>
                                <p>Reference project</p><a href="en/references/csg-ts/" class="more">&nbsp;</a>
                            </div>
                        </div>
                        <div id="c1623" class="">
                            <h3>CSG-TS</h3>
                            <p>Managing 13,000 branches, post agencies and post service shops, more than
                                100,000&nbsp;letterboxes, 1000 paketbox stations and 2500 packstations is a daily
                                challenge for CSG.TS GmbH: ensuring the functionality of the world’s biggest logistics
                                and postal company. We support them by providing a targeted analysis and conception,
                                with structuring and a catalogue of measures and with a holistic implementation concept
                                for the sustainable optimisation.</p>
                        </div>
                        <Button variant="outlined" color="primary" type="submit" onClick={handleClick}>
                          Start
                        </Button>
                </div>
                </div>
            </div>
    );

  // return (
  //   <div className="joinOuterContainer">
  //     <div className="joinInnerContainer">
  //       <h1 className="heading">progress of Game</h1>
  //       <CounterDown startNumber={startNumber} />
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <Link onClick={(e) => ((!name || !room) ? e.preventDefault() : null)} to={`/chat?name=${name}&room=${room}`}>*/}
  //     {/*    <button className="button mt-20" type="submit">Sign In</button>*/}
  //     {/*  </Link>*/}
  //     </div>
  //   </div>
  // );
};

export default Introduction;
