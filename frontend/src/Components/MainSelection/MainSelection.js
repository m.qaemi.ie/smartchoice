import "./MainSelection.css";
import FormsyInput from "../FormsyInput";
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';


import Formsy from 'formsy-react';
import React, { useState, useEffect } from 'react';
// import RadioButton from "../RadioButton";
import RadioButtons from '../MaterialUiComponents/RadioButtons'; 
import CheckboxesGroup from '../MaterialUiComponents/CheckBoxes'; 
import ImgMediaCard from '../MaterialUiComponents/ImgMediaCard';
import TextFields from '../MaterialUiComponents/TextFields';
import EnhancedTable from '../MaterialUiComponents/EnhancedTable';
import {textFieldsQuestionsList, checkboxesQuestionsListMainSelection} from '../QuestionsList';
import { makeStyles } from '@material-ui/core/styles';

const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
      display: 'block-inline'
    },
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginTop: '2%',
    marginBottom: '2%',
    paddingTop: '5%',
    paddingBottom: '5%'
  },
  title:{
    marginBottom: '3%',
    marginLeft: '3%',
    // float:'left'
  }
}));
 
const MainSelection = ({locationType, systemType, equipmentType, recommendedDeviceComponent}) => {
  const [canSubmit, setCanSubmit] = useState(false);
  const [inputsValues, setInputsValues] = useState({equipmentType});
  // const [recommendedDevice, setRecommendedDevice] = useState({});
  const [recommendedProductComponent, setRecommendedProductComponent] = useState('');
  const classes = useStyles();

  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);

  const disableButton = () => {
    setCanSubmit(false);
  };
 
  const enableButton = () => {
    setCanSubmit(true);
  };

  // function createData(ProductNo, COPoderEER, ChilledWaterOutletTemperaturC, CoolCapacityKW, CoolingWaterOutletTemperaturC, EcologicalTarget, EquipmentType, Manufacturer, PriceRange) {
  //   return { ProductNo, COPoderEER, ChilledWaterOutletTemperaturC, CoolCapacityKW, CoolingWaterOutletTemperaturC, EcologicalTarget, EquipmentType, Manufacturer, PriceRange };
  // }
  //   function createData(name, calories, fat, carbs, protein) {
  //   return { name, calories, fat, carbs, protein };
  // }
  
  // const rows = [
  //   createData('Cupcake', 305, 3.7, 67, 4.3),
  //   createData('Donut', 452, 25.0, 51, 4.9),
  //   createData('Eclair', 262, 16.0, 24, 6.0),
  //   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  //   createData('Gingerbread', 356, 16.0, 49, 3.9),
  //   createData('Honeycomb', 408, 3.2, 87, 6.5),
  //   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  //   createData('Jelly Bean', 375, 0.0, 94, 0.0),
  //   createData('KitKat', 518, 26.0, 65, 7.0),
  //   createData('Lollipop', 392, 0.2, 98, 0.0),
  //   createData('Marshmallow', 318, 0, 81, 2.0),
  //   createData('Nougat', 360, 19.0, 9, 37.0),
  //   createData('Oreo', 437, 18.0, 63, 4.0),
  // ];
//   const headCells = [
//   { id: 'name', numeric: false, disablePadding: true, label: 'Dessert (100g serving)' },
//   { id: 'calories', numeric: true, disablePadding: false, label: 'Calories' },
//   { id: 'fat', numeric: true, disablePadding: false, label: 'Fat (g)' },
//   { id: 'carbs', numeric: true, disablePadding: false, label: 'Carbs (g)' },
//   { id: 'protein', numeric: true, disablePadding: false, label: 'Protein (g)' },
// ];

  
  const headCells = [
    { id: 'ProductNo', numeric: false, disablePadding: true, label: 'ProductNo' },
    { id: 'COPoderEER', numeric: false, disablePadding: false, label: 'COPoderEER' },
    { id: 'ChilledWaterOutletTemperaturC', numeric: false, disablePadding: false, label: 'ChilledWaterOutletTemperaturC' },
    { id: 'CoolCapacityKW', numeric: false, disablePadding: false, label: 'CoolCapacityKW' },
    { id: 'CoolingWaterOutletTemperaturC', numeric: false, disablePadding: false, label: 'CoolingWaterOutletTemperaturC' },
    { id: 'EcologicalTarget', numeric: false, disablePadding: false, label: 'EcologicalTarget' },
    { id: 'EquipmentType', numeric: false, disablePadding: false, label: 'EquipmentType' },
    { id: 'Manufacturer', numeric: false, disablePadding: false, label: 'Manufacturer' },
    { id: 'PriceRange', numeric: false, disablePadding: false, label: 'PriceRange' },
  ];


  const submit = (event) => {
    event.preventDefault();
    axios.post(`http://${process.env.REACT_APP_IP_ADDRESS_OF_SERVER}:${process.env.REACT_APP_PORT_OF_SERVER}/mainSelectionForm`, {
        // model: event.target.value
        model: inputsValues
      },
    //   { 
    //     method: 'POST',
    //     mode: 'cors',
    //     headers: { 
    //     'Access-Control-Allow-Origin': 'http://localhost:3000',
    //     'Access-Control-Allow-Credentials': true 
    // }, 
    // }
    )
      .then(function (response) {
        console.log(response);
        // setRecommendedDevice(response.data['recommended device']);
        // setRecommendedProductComponent(<ImgMediaCard imgCardOptions={response.data['recommended device']} />);
        // setRecommendedProductComponent(<div>{response.data['recommendedProductNo']}</div>);
        const rows = response.data['recommendedProductNo']
        setRecommendedProductComponent(<EnhancedTable headCells={headCells} rows={rows} />);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
    const textFieldsQuestions = textFieldsQuestionsList.map((options, index) => {
      const question = (
        <div key={index}>
        <List>
            <ListItem divider>
              <ListItemText primary={options.question} />
              <TextFields field={options.fields[0]} setInputsValues={setInputsValues} />
              <TextFields field={options.fields[1]} setInputsValues={setInputsValues} />
            </ListItem>
        </List>
        </div>
      );
      return question
    });
 
    const checkboxesQuestions = checkboxesQuestionsListMainSelection.map((checkboxOptions, index) => {
      return (
        <List>
            <ListItem divider={index!==checkboxesQuestionsListMainSelection.length-1?true:false}>
              <ListItemText primary={checkboxOptions.question} />
              <CheckboxesGroup key={index} checkboxOptions={checkboxOptions.checkboxes} setInputsValues={setInputsValues} question={checkboxOptions.question}/>            </ListItem>
        </List>
        );
    });

    return (
      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
          <Grid  item xs={1}/>
          <Grid item xs={10}>
            <Paper className={classes.paper} elevation={3}>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
              {/* "Equipment Type(s)" recommended to you based on information provided: */}
              Recommended "Equipment Type(s)":
            </Typography>
            {recommendedDeviceComponent}
            </Paper>
          </Grid>
          <Grid  item xs={1}/>
        </Grid>
        <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
          <Grid  item xs={1}/>
          <Grid item xs={10}>
            <Paper className={classes.paper} elevation={3}>
              <Typography className={classes.title} variant="h6" id="formTitle" component="div">
                Complete this form so that we can help you select "product":
              </Typography>
              <form className={classes.root} noValidate autoComplete="off" onSubmit={submit}>
                {textFieldsQuestions}
                {checkboxesQuestions}
                <Button variant="outlined" color="primary" type="submit">
                  Submit
                </Button>
                {recommendedProductComponent}
              </form>
            </Paper>
          </Grid>
          <Grid  item xs={1}/>
        </Grid>
      </div>
    );
};
export default MainSelection;
