import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
// import Button from '@material-ui/core/Button';
import ImgMediaCard from '../MaterialUiComponents/ImgMediaCard';
import { makeStyles } from '@material-ui/core/styles';
const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginTop: '2%',
    marginBottom: '2%',
    paddingTop: '5%',
    paddingBottom: '5%'
  },
  title:{
    marginBottom: '3%',
    marginLeft: '3%',
    float:'left'
  }
}));


const SelectLocationType = ({setStage, setLocationType}) => {
  const classes = useStyles();
  const [imgCards, setImgCards] = useState('');

  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);
  
    useEffect(() => {
    // Make a request for get location types
    axios.get(`http://${process.env.REACT_APP_IP_ADDRESS_OF_SERVER}:${process.env.REACT_APP_PORT_OF_SERVER}/LocationTypes`)
    .then(function (response) {
      // handle success
      console.log(response);
      const imgCardOptionsList = response.data.map((locationType) => { 
        return {
          media:{
            alt: locationType.name,
            height:"140",
            imgUrl:locationType.abstractimageoflocationtype_set[0].file,
            title: locationType.name
          },
          title:locationType.name,
          body: locationType.description
        }
      });
      
      const imgCards = imgCardOptionsList.map((imgCardOptions, index) => {
        return <ImgMediaCard imgCardOptions={imgCardOptions} key={index} handleClick={handleClick}/>;
      });
      setImgCards(imgCards);      
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  }, []);
    const handleClick = (locationType) => {
      setLocationType(locationType);
      setStage('SelectSystemType');
    };
    // const imgCards = imgCardOptionsList.map((imgCardOptions, index) => {
    //   return <ImgMediaCard imgCardOptions={imgCardOptions} key={index} handleClick={handleClick}/>;
    // });

    const submit = (event) => {
        event.preventDefault();
        setStage('SelectSystemType');
        // axios.post('http://localhost:8000/preSelectionForm', {
        //     // model: event.target.value
        //     model: inputsValues
        //   },
        //   { 
        //     method: 'POST',
        //     mode: 'cors',
        //     headers: { 
        //     'Access-Control-Allow-Origin': 'http://localhost:3000',
        //     'Access-Control-Allow-Credentials': true 
        // }, 
        // })
        //   .then(function (response) {
        //     console.log(response);
        //     setRecommendedDevice(response.data['recommended device']);
        //   })
        //   .catch(function (error) {
        //     console.log(error);
        //   });
      };
     
    return (
        <div className={classes.root}>
          <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
            <Grid container item xs={1}>
              {/* <Paper className={classes.paper}></Paper> */}
            </Grid>
            {/* <Grid container item direction="row" justify="center" alignItems="center" xs={10}> */}
            <Grid container item xs={10}>
              <Paper className={classes.paper} elevation={3}>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Select "Location Type" that equipment will be used:
              </Typography>
                {/* <Grid container item direction="row" justify="center" alignItems="center" xs={10}> */}
                <Grid container item xs={12} spacing={0}>
                  {imgCards.slice(0, 4)}
                </Grid>
                <Grid container item xs={12} marginTop={3} spacing={0}>
                  {imgCards.slice(4, imgCards.length+1)}
                </Grid>
                {/* </Grid>                                 */}
              </Paper>
            </Grid>
            <Grid container item xs={1}>
              {/* <Paper className={classes.paper}></Paper> */}
            </Grid>
          </Grid>
        </div>
    );
    
  // return (
  //   <div className="joinOuterContainer">
  //     <div className="joinInnerContainer">
  //       <h1 className="heading">progress of Game</h1>
  //       <CounterDown startNumber={startNumber} />
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <Link onClick={(e) => ((!name || !room) ? e.preventDefault() : null)} to={`/chat?name=${name}&room=${room}`}>*/}
  //     {/*    <button className="button mt-20" type="submit">Sign In</button>*/}
  //     {/*  </Link>*/}
  //     </div>
  //   </div>
  // );
};

export default SelectLocationType;
