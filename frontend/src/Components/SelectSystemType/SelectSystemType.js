import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import ImgMediaCard from '../MaterialUiComponents/ImgMediaCard';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginTop: '2%',
    marginBottom: '2%',
    paddingTop: '5%',
    paddingBottom: '5%'
  },
  title:{
    marginBottom: '3%',
    marginLeft: '3%',
    float:'left'
  }
}));


const SelectSystemType = ({setStage, setSystemType ,locationType}) => {
  const classes = useStyles();
  const [imgCards, setImgCards] = useState('');

  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);
  
  useEffect(() => {
    // Make a request for get location types
    axios.get(`http://${process.env.REACT_APP_IP_ADDRESS_OF_SERVER}:${process.env.REACT_APP_PORT_OF_SERVER}/SystemTypes`)
    .then(function (response) {
      // handle success
      console.log(response);
      const imgCardOptionsList = response.data.map((systemType) => { 
        return {
          media:{
            alt: systemType.name,
            height:"140",
            imgUrl: systemType.abstractimageofsystemtype_set[0].file,
            title: systemType.name
          },
          title:systemType.name,
          body: systemType.description
        }
      });
      
      const imgCards = imgCardOptionsList.map((imgCardOptions, index) => {
        return <ImgMediaCard imgCardOptions={imgCardOptions} key={index} handleClick={handleClick}/>;
      });
      setImgCards(imgCards);      
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
  }, []);

    const handleClick = (systemType) => {
      setSystemType(systemType);
      setStage('PreSelection');
    };

    const submit = (event) => {
        event.preventDefault();
        setStage('PreSelection');
      };
     
      return (
        <div className={classes.root}>
          <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
            <Grid container item xs={1}>
              {/* <Paper className={classes.paper}></Paper> */}
            </Grid>
            {/* <Grid container item direction="row" justify="center" alignItems="center" xs={10}> */}
            <Grid container item xs={10}>
              <Paper className={classes.paper} elevation={3}>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Select "System Type" you need:
              </Typography>

                {/* <Grid container item direction="row" justify="center" alignItems="center" xs={10}> */}
                {/* <Grid container item xs={12} spacing={0}>
                  {imgCards.slice(0, 4)}
                </Grid> */}
                <Grid container item xs={12} marginTop={3} spacing={0}>
                  {imgCards.slice(0, imgCards.length+1)}
                </Grid>
                {/* </Grid>                                 */}
              </Paper>
            </Grid>
            <Grid container item xs={1}>
              {/* <Paper className={classes.paper}></Paper> */}
            </Grid>
          </Grid>
        </div>
    );
};

export default SelectSystemType;
