import "./PreSelection.css";
import FormsyInput from "../FormsyInput";
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import Formsy from 'formsy-react';
import React, { useState, useEffect } from 'react';
import RadioButtons from '../MaterialUiComponents/RadioButtons'; 
import CheckboxesGroup from '../MaterialUiComponents/CheckBoxes'; 
import ImgMediaCard from '../MaterialUiComponents/ImgMediaCard';
import {radioButtonsQuestionsList, checkboxesQuestionsList} from '../QuestionsList';
const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginTop: '2%',
    marginBottom: '2%',
    paddingTop: '5%',
    paddingBottom: '5%'
  },
  title:{
    marginBottom: '3%',
    marginLeft: '3%',
    float:'left'
  }
}));


const PreSelection = ({setStage, locationType, systemType, setEquipmentType, setRecommendedDeviceComponent}) => {
  const classes = useStyles();
  const [canSubmit, setCanSubmit] = useState(false);
  const [inputsValues, setInputsValues] = useState({});
  // const [recommendedDeviceComponent, setRecommendedDeviceComponent] = useState('');

  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);

  const disableButton = () => {
    setCanSubmit(false);
  };
 
  const enableButton = () => {
    setCanSubmit(true);
  };

  const handleClick = (equipmentType) => {
    setEquipmentType(equipmentType);
    setStage('MainSelection');
  };
 
  const submit = (event) => {
    event.preventDefault();
    axios.post(`http://${process.env.REACT_APP_IP_ADDRESS_OF_SERVER}:${process.env.REACT_APP_PORT_OF_SERVER}/preSelectionForm`, {
        // model: event.target.value
        model: inputsValues
      },
    //   { 
    //     method: 'POST',
    //     mode: 'cors',
    //     headers: { 
    //     'Access-Control-Allow-Origin': 'http://localhost:3000',
    //     'Access-Control-Allow-Credentials': true 
    // }, 
    // }
    )
      .then(function (response) {
        console.log(response);
        setRecommendedDeviceComponent(<ImgMediaCard imgCardOptions={response.data['recommended device']} handleClick={handleClick}/>);
        setEquipmentType(response.data['recommended device'].title);
        setStage('MainSelection')
      })
      .catch(function (error) {
        console.log(error);
      });
  };
    const radioButtonsQuestions = radioButtonsQuestionsList.map((radioButtonOptions, index) => {
      return (
        <List>
            <ListItem divider>
              <ListItemText primary={radioButtonOptions.question} />
              <RadioButtons radioOptions={radioButtonOptions.radioButtons} setInputsValues={setInputsValues} name={radioButtonOptions.name} />
            </ListItem>
        </List>
        );
    });
 
    const checkboxesQuestions = checkboxesQuestionsList.map((checkboxOptions, index) => {
      return (
        <List>
        <ListItem divider={index!==checkboxesQuestionsList.length-1?true:false}>
          <ListItemText primary={checkboxOptions.question} />
            <CheckboxesGroup setInputsValues={setInputsValues} checkboxOptions={checkboxOptions.checkboxes} />
            </ListItem>
        </List>
      );
    });

    return (
      <div className={classes.root}>
        <Grid container direction="row" justify="center" alignItems="center" spacing={2}>
          <Grid  item xs={1}>
            {/* <Paper className={classes.paper}></Paper> */}
          </Grid>
          {/* <Grid container item direction="row" justify="center" alignItems="center" xs={10}> */}
          <Grid item xs={10}>
            <Paper className={classes.paper} elevation={3}>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                Complete this form so that we can help you select "Equipment Type":
            </Typography>
              <form className="form-horizontal" onSubmit={submit}>
                {radioButtonsQuestions}
                {checkboxesQuestions}
                <Button variant="outlined" color="primary" type="submit">
                  Submit
                </Button>
                {/* {recommendedDeviceComponent} */}
              </form>
            </Paper>
          </Grid>
          <Grid  item xs={1}>
            {/* <Paper className={classes.paper}></Paper> */}
          </Grid>
        </Grid>
      </div>
  );
};
export default PreSelection;
