import { withFormsy } from 'formsy-react';
import React from 'react';
 
const RadioButton = (props) => {
 
  const changeValue = (event) => {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    props.setValue(event.currentTarget.value);
  };
 
    // An error message is passed only if the component is invalid
    const errorMessage = props.errorMessage;
 
    return (
      // <div>
          <label className="radio-inline" for={`radios-${props.sequence}`}>
          <input type="radio" name={props.name} id={`radios-${props.sequence}`} value={props.value} />
              {props.value}
          </label> 

        // {/* <input onChange={changeValue} type="text" value={props.value || ''} />
        // <span>{errorMessage}</span>
      // </div>
    );
};

export default withFormsy(RadioButton);
