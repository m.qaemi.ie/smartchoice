import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles({
  root: {
    height: '100%',
    maxWidth: '100%',
    // display: 'inline-block',
    margin: '4%',
    paddingTop: '4%'
  },
  img: {
    width: '100%',
    height: 'auto',
  },
  // paper: {
    // height: 
    // padding: theme.spacing(2),
    // textAlign: 'center',
    // color: theme.palette.text.secondary,
    // marginTop: '2%',
    // marginBottom: '2%',
    // paddingTop: '5%',
    // paddingBottom: '5%'
  // },
});

export default function ImgMediaCard({imgCardOptions, handleClick}) {
  const classes = useStyles();

  return (
    // <Grid container direction="row" justify="center" alignItems="center" spacing={2} >
     <Grid item xs={3} spacing={2}>
      {/* <Paper elevation={3}> */}
      <Card className={classes.root} onClick={() => handleClick(imgCardOptions.title)}>
        <CardActionArea>
          <CardMedia
            className={classes.img}
            component="img"
            alt={imgCardOptions.media.alt}
            // height={imgCardOptions.media.height}
            image={imgCardOptions.media.imgUrl}
            title={imgCardOptions.media.title}
          />
          <CardContent>
            {/* <Typography gutterBottom variant="h5" component="h2" style={{ backgroundColor: '#cfe8fc', wra }}> */}
            <Typography gutterBottom variant="h5" component="h2" >
              {imgCardOptions.title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p" >
              {imgCardOptions.body}
            </Typography>
          </CardContent>
        </CardActionArea>
        {/* <CardActions> */}
          {/* <Button size="small" color="primary">
            Share
          </Button> */}
          {/* <Button size="small" color="primary">
            Learn More
          </Button> */}
        {/* </CardActions> */}
      </Card>
      {/* </Paper> */}
     </Grid>
    //  </Grid>
  );
}
