import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
import HorizontalLabelPositionBelowStepper from '../MaterialUiComponents/HorizontalLabelPositionBelowStepper';

import Introduction from '../Introduction/Introduction';
import SelectLocationType from '../SelectLocationType/SelectLocationType';
import SelectSystemType from '../SelectSystemType/SelectSystemType';
import PreSelection from '../PreSelection/PreSelection';
import MainSelection from '../MainSelection/MainSelection';

const Stages = () => {
    const [stage, setStage] = useState('Introduction');
    const [locationType ,setLocationType] = useState('');
    const [systemType ,setSystemType] = useState('');
    const [equipmentType, setEquipmentType] = useState('');
    const [recommendedDeviceComponent, setRecommendedDeviceComponent] = useState('');

    if (stage === 'Introduction') {
        return <Introduction setStage={setStage}/>;        
    } else if (stage === 'SelectLocationType') {
        return (
        <div>
            <HorizontalLabelPositionBelowStepper
                steps={[{label:'Building Type Selection', result:locationType}, {label:'System Type Selection', result:systemType}, {label:'Equipment Type Selection', result:equipmentType}, {label:'Product Number Selection', result:''}]}
                activeStep={0}
            />
            <SelectLocationType setStage={setStage} setLocationType={setLocationType}/>
        </div>
            );
    } else if (stage === 'SelectSystemType') {
        return (
            <div>
                <HorizontalLabelPositionBelowStepper
                    steps={[{label:'Building Type Selection', result:locationType}, {label:'System Type Selection', result:systemType}, {label:'Equipment Type Selection', result:equipmentType}, {label:'Product Number Selection', result:''}]}
                    activeStep={1}
                />
                <SelectSystemType setStage={setStage} locationType={locationType} setSystemType={setSystemType}/>
            </div>
                );    
    } else if (stage === 'PreSelection') {
        return (
            <div>
                <HorizontalLabelPositionBelowStepper
                    steps={[{label:'Building Type Selection', result:locationType}, {label:'System Type Selection', result:systemType}, {label:'Equipment Type Selection', result:equipmentType}, {label:'Product Number Selection', result:''}]}
                    activeStep={2}
                />
                <PreSelection setStage={setStage} locationType={locationType} systemType={systemType} setEquipmentType={setEquipmentType} setRecommendedDeviceComponent={setRecommendedDeviceComponent}/>
            </div>
                );    
    } else if (stage === 'MainSelection') {
        return (
            <div>
                <HorizontalLabelPositionBelowStepper
                    steps={[{label:'Building Type Selection', result:locationType}, {label:'System Type Selection', result:systemType}, {label:'Equipment Type Selection', result:equipmentType}, {label:'Product Number Selection', result:''}]}
                    activeStep={2}
                />
                <MainSelection setStage={setStage} locationType={locationType} systemType={systemType} equipmentType={equipmentType} recommendedDeviceComponent={recommendedDeviceComponent}/>
            </div>
                );
    } 

  // return (
  //   <div className="joinOuterContainer">
  //     <div className="joinInnerContainer">
  //       <h1 className="heading">progress of Game</h1>
  //       <CounterDown startNumber={startNumber} />
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Name" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <div>*/}
  //     {/*    <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />*/}
  //     {/*  </div>*/}
  //     {/*  <Link onClick={(e) => ((!name || !room) ? e.preventDefault() : null)} to={`/chat?name=${name}&room=${room}`}>*/}
  //     {/*    <button className="button mt-20" type="submit">Sign In</button>*/}
  //     {/*  </Link>*/}
  //     </div>
  //   </div>
  // );
};

export default Stages;
