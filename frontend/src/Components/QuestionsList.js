const importance_levels_value_label = [
    {value: '1', label: 'very low'},
    {value: '2', label: 'low'},
    {value: '3', label: 'moderate'},
    {value: '4', label: 'high'},
    {value: '5', label: 'extreme high'},
    ];

const yes_no_value_label = [
    {value: '5', label: 'Yes'},
    {value: '1', label: 'No'},
    // {value: '3', label: 'moderate'},
    // {value: '4', label: 'high'},
    // {value: '5', label: 'extreme'},
    ];
    // Range of cool load 3-20 kW;Range of cool load 20-50 kW;Range of cool load 50-200 kW;Range of cool load 200-1000 kW;Range of cool load 1000-5000 kW;Water;Gas;Electricity

const radioButtonsQuestionsList = [
    {   
        radioButtons: importance_levels_value_label,
        name: 'Minimum initial cost',
        question: 'The "Minimum initial cost" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Minimum Annual O&M cost',
        question: 'The "Minimum Annual O & M cost" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Minimum annual energy cost (bills)',
        question: 'The "Minimum annual energy cost (bills)" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Maximum lifespan',
        question: 'The "Maximum lifespan" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Speed  of respond to load',
        question: 'The "Speed  of respond to load" importance level:'
    },
    {
        radioButtons: importance_levels_value_label,
        name: 'Cooling and heating simultaneously',
        question: 'The "Having cooling and heating simultaneously" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Environment-friendly components',
        question: 'The "Environment-friendly components" importance level:'
    },
    {   
        radioButtons: importance_levels_value_label,
        name: 'Minimum CO2 emission',
        question: 'The "Minimum CO2 emission" importance level:'
    },
    {   
        radioButtons: [
        {value: '1', label: 'B'},
        {value: '2', label: 'A'},
        {value: '4', label: 'A+'},
        {value: '5', label: 'A++'},
        {value: '6', label: 'A+++'},
        ],
        name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'The Level of "Eco-label"(Green Building norms):'
    },
    {   
        radioButtons: [
        {value: '1', label: '10-16 °C'},
        {value: '5', label: '6-12 °C'},
        ],
        name: 'Temperatur level [°C]=>6-12°C=1=>10-16°C =0',
        question: 'The "Temperatur level" corresponds to the equipment:'
    },
    {   
        radioButtons: yes_no_value_label,
        name: 'Is the equipment flexible about type of fan-coil',
        question: 'The equipment should be flexible to type of fan-coil:'
    },
    {   
        radioButtons: [
            {value: '5', label: 'very small space'},
            {value: '4', label: 'small space'},
            {value: '1', label: 'space does not matter.'},
            // {value: '1', label: 'very large'},
            ],
        name: 'The equipment does not need any large space',
        question: 'The equipment requires:'
    },
    {   
        radioButtons: yes_no_value_label,
        name: 'More than 80%',
        question: 'The moisture of location is more than 80%?'
    },
];

const checkboxesQuestionsList = [
    {   
        checkboxes: [
            {value:false, label:'Gas', name:'Gas'},
            {value:false, label:'Water', name:'Water'},
            {value:false, label:'Electricity', name:'Electricity'}
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'Resources can be used:'
    },
    {
        checkboxes: [
            {value:false, label:'3-20 kW', name:'Range of cool load 3-20 kW'},
            {value:false, label:'20-50 kW', name:'Range of cool load 20-50 kW'},
            {value:false, label:'50-200 kW', name:'Range of cool load 50-200 kW'},
            {value:false, label:'200-1000 kW', name:'Range of cool load 200-1000 kW'},
            {value:false, label:'1000-5000 kW', name:'Range of cool load 1000-5000 kW'}
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'Cool load range:'
    },
];

const checkboxesQuestionsListMainSelection = [
    {   
        checkboxes: [
            {value:false, label:'Carrier', name:'Carrier'},
            {value:false, label:'York', name:'York'},
            {value:false, label:'Daikin', name:'Daikin'}
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'Manufacturer:'
    },
];

const textFieldsQuestionsList = [
    {   
        fields: [
            {value:'', label:'Min', name:'CoolCapacityKWLowerBound'},
            {value:'', label:'Max', name:'CoolCapacityKWUpperBound'},
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'The range of cool Capacity(KW):'
    },
    {   
        fields: [
            {value:'', label:'Min', name:'ChilledWaterOutletTemperaturCLowerBound'},
            {value:'', label:'Max', name:'ChilledWaterOutletTemperaturCUpperBound'},
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'The range of chilled water outlet temperature(°C):'
    },
    {   
        fields: [
            {value:'', label:'Min', name:'CoolingWaterOutletTemperaturCLowerBound'},
            {value:'', label:'Max', name:'CoolingWaterOutletTemperaturCUpperBound'},
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'The range of cooling water outlet temperature(°C):'
    },
    {   
        fields: [
            {value:'', label:'Min', name:'PriceRangeLowerBound'},
            {value:'', label:'Max', name:'PriceRangeUpperBound'},
        ],
        // name: 'Best rate of Eco-labeling(Green Building norms)',
        question: 'The range of Price(€):'
    },
];

export {radioButtonsQuestionsList, checkboxesQuestionsList, textFieldsQuestionsList, checkboxesQuestionsListMainSelection};