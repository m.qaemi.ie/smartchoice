import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import PreSelection from '../src/Components/PreSelection/PreSelection';
import Stages from '../src/Components/Stages/Stages'

ReactDOM.render(
  // <React.StrictMode>
  <Stages/>
    // <PreSelection />
  // </React.StrictMode>,
  ,document.querySelector('#root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
