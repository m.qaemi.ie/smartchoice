import sqlite3
import pandas as pd
from sqlalchemy import create_engine

file = 'coding-Main-Selection/Main Selection step_Dataset2.xlsx'
# output = 'output.xlsx'

engine = create_engine('sqlite://', echo=False)
df = pd.read_excel(file, sheet_name='MainSelectionDataset')
df.to_sql('cooling', engine, if_exists='replace', index=False)

EquipmentType = 'Absorption Chiller Single Effect'
Manufacturer = 'Carrier'
CoolCapacityKWUpperBound = 150
ChilledWaterOutletTemperaturCUpperBound = 8
CoolingWaterOutletTemperaturCUpperBound = 32
query_to_database = f"Select * from cooling where \
EquipmentType='{EquipmentType}' \
AND Manufacturer='{Manufacturer}' \
AND CoolCapacityKW<{CoolCapacityKWUpperBound} \
AND ChilledWaterOutletTemperaturC<{ChilledWaterOutletTemperaturCUpperBound} \
AND CoolingWaterOutletTemperaturC<{CoolingWaterOutletTemperaturCUpperBound} "

results = engine.execute(query_to_database)

final = pd.DataFrame(results, columns=df.columns)
# final.to_excel(output, index=False)
