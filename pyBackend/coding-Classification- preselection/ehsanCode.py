import pandas as pd

DATA = pd.read_csv(r"0&1.csv", sep=';')
DATA = DATA[:259]

test_df = DATA.head(2)  # adding equipment 1
test_df = test_df.append(DATA[10:13])  # adding equipment 2
test_df = test_df.append(DATA[23:26])  # adding equipment 3
test_df = test_df.append(DATA[38:41])  # adding equipment 4
test_df = test_df.append(DATA[52:57])  # adding equipment 5
test_df = test_df.append(DATA[73:76])  # adding equipment 6
test_df = test_df.append(DATA[88:93])  # adding equipment 7
test_df = test_df.append(DATA[112:115])  # adding equipment 8
test_df = test_df.append(DATA[126:128])  # adding equipment 9
test_df = test_df.append(DATA[140:141])  # adding equipment 10
test_df = test_df.append(DATA[145:146])  # adding equipment 11
test_df = test_df.append(DATA[150:158])  # adding equipment 12
test_df = test_df.append(DATA[187:193])  # adding equipment 13
test_df = test_df.append(DATA[217:219])  # adding equipment 14
test_df = test_df.append(DATA[233:236])  # adding equipment 15
test_df = test_df.append(DATA[251:253])  # adding equipment 16

train_df = DATA[2:10]  # adding equipment 1
train_df = train_df.append(DATA[13:23])  # adding equipment 2
train_df = train_df.append(DATA[26:38])  # adding equipment 3
train_df = train_df.append(DATA[41:52])  # adding equipment 4
train_df = train_df.append(DATA[57:73])  # adding equipment 5
train_df = train_df.append(DATA[76:88])  # adding equipment 6
train_df = train_df.append(DATA[93:112])  # adding equipment 7
train_df = train_df.append(DATA[115:126])  # adding equipment 8
train_df = train_df.append(DATA[128:140])  # adding equipment 9
train_df = train_df.append(DATA[141:145])  # adding equipment 10
train_df = train_df.append(DATA[146:150])  # adding equipment 11
train_df = train_df.append(DATA[158:187])  # adding equipment 12
train_df = train_df.append(DATA[193:217])  # adding equipment 13
train_df = train_df.append(DATA[219:233])  # adding equipment 14
train_df = train_df.append(DATA[236:251])  # adding equipment 15
train_df = train_df.append(DATA[253:259])  # adding equipment 16

train_x = train_df.drop('Equipment No.', 1)
train_y = train_df['Equipment No.']

# seperate the independent and target variable on testing data
test_x = test_df.drop('Equipment No.', 1)
test_y = test_df['Equipment No.']

import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score

from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification

model = RandomForestClassifier()
model.fit(train_x, train_y)

# predict the target on the train dataset
predict_train = model.predict(train_x)
print('\nTarget on train data', predict_train)

# Accuray Score on train dataset
accuracy_train = accuracy_score(train_y, predict_train)
print('\naccuracy_score on train dataset : ', accuracy_train)

# predict the target on the test dataset
predict_test = model.predict(test_x)
print('\nTarget on test data', predict_test)

# Accuracy Score on test dataset
accuracy_test = accuracy_score(test_y, predict_test)
print('\naccuracy_score on test dataset : ', accuracy_test)

import pickle

# save the model to disk
filename = 'finalized_DT_model.sav'
pickle.dump(model, open(filename, 'wb'))

X1_test = pd.read_csv(r"1)Bitte geben Sie die Anforderungen an.csv", sep=';')

# load the model from disk
loaded_model = pickle.load(open(filename, 'rb'))
result = loaded_model.predict(X1_test)
print(result)

if result == [1]:
    a = "Absorption Chiller Single Effect"

elif result == [2]:
    a = "Absorption Chiller Double Effect"

elif result == [3]:
    a = "Absorption  Chiller Direct Fire"

elif result == [4]:
    a = "Water cooled centrifugal compressor chiller"

elif result == [5]:
    a = "Air cooled Centrifugal compressor chiller"

elif result == [6]:
    a = " Water cooled screw compressor chiller"

elif result == [7]:
    a = "Air cooled screw compressor chiller"

elif result == [8]:
    a = "Water cooled scroll compressor chiller"

elif result == [9]:
    a = "Air cooled scroll compressor chiller"

elif result == [10]:
    a = "Water cooled reciprocating compressor chiller"

elif result == [11]:
    a = "Air cooled Reciprocating compressor chiller"

elif result == [12]:
    a = "Heat Pump - Water"

elif result == [13]:
    a = "Heat Pump - Geothermal"

elif result == [14]:
    a = "AC Split"

elif result == [15]:
    a = "VRF or VRV"

elif result == [16]:
    a = "Evaporative cooler (Verdunstungskühlung)"

from tkinter import *

window = Tk()

window.title("Ergebnisse von HLK-Auswahl Tool")
window.geometry('1200x900')

lbl1 = Label(window, text="M+P Braunschweig GmbH", font=("Arial Bold", 25), fg="navy blue")
lbl1.grid(column=0, row=0)

lbl2 = Label(window, text="CREATING ADDED VALUE", font=("Arial Bold", 10), fg="navy blue")
lbl2.grid(column=0, row=1)

lbl0 = Label(window, text="    ", font=("Arial Bold", 10))
lbl0.grid(column=1, row=1)

lbl00 = Label(window, text="    ", font=("Arial Bold", 10))
lbl00.grid(column=1, row=2)

lbl000 = Label(window, text="    ", font=("Arial Bold", 10))
lbl000.grid(column=1, row=3)

lbl3 = Label(window, text="Art der Ausrüstung:", font=("Arial Black", 18))
lbl3.grid(column=1, row=5)

lbl3_1 = Label(window, text=a, font=("Arial Bold", 18))
lbl3_1.grid(column=2, row=5)

lbl4 = Label(window, text="Produkt-Nr:", font=("Arial Black", 18))
lbl4.grid(column=1, row=6)

lbl5 = Label(window, text="Hersteller:", font=("Arial Black", 18))
lbl5.grid(column=1, row=7)

lbl6 = Label(window, text="technisches Datenblatt:", font=("Arial Black", 18))
lbl6.grid(column=1, row=8)

lbl6_1 = Label(window, text="Bitte klicken Sie hier", font=("Arial Bold", 10), fg="blue")
lbl6_1.grid(column=2, row=8)

lbl6 = Label(window, text="technisches Datenblatt:", font=("Arial Black", 18))
lbl6.grid(column=1, row=8)

lbl6 = Label(window, text="technisches Datenblatt:", font=("Arial Black", 18))
lbl6.grid(column=1, row=8)

lbl6 = Label(window, text="technisches Datenblatt:", font=("Arial Black", 18))
lbl6.grid(column=1, row=8)

window.mainloop()

