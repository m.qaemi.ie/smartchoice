from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from creators.models import CommonFieldForAllModels
from django.contrib.postgres.fields import JSONField, ArrayField
from django.core.serializers.json import DjangoJSONEncoder

from django.contrib.auth import get_user_model

import uuid

from django.utils.safestring import mark_safe

# from tinymce.models import HTMLField
# from ckeditor.fields import RichTextField


class Spectator(CommonFieldForAllModels):  # Spectator can be ProjectUser or Device only.
    projectUser = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)


class Video(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.FileField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True


class Image(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class ImageOfCarouselOfEquipment(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=False, null=True)
    # class Meta:
    #     abstract = True

    def __str__(self):
        return self.title

    def image_tag(self):
        return mark_safe('<img src="%s" />' % self.file.url)
    image_tag.short_description = 'Image'


class Audio(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.FileField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True


class Category(CommonFieldForAllModels):
    title = models.CharField(max_length=100, blank=False, null=False, unique=True)

    def __str__(self):
        return '%s' % self.title


class Equipment(CommonFieldForAllModels):
    author = models.ForeignKey(get_user_model(), related_name='author', on_delete=models.DO_NOTHING, blank=True, null=True)
    # video = models.ManyToManyField(Video)
    # image = models.ManyToManyField(Image)
    # audio = models.ManyToManyField(Audio)
    name = models.CharField(max_length=100, blank=False, null=False)
    about = models.CharField(max_length=1000, blank=False, null=False)
    # content = HTMLField(blank=True, null=True)
    articleAboutEquipment = RichTextUploadingField(blank=True, null=True)
    # userLikesEquipment = models.ManyToManyField(get_user_model(), related_name='userLikeEquipment')
    category = models.ManyToManyField(Category)
    # images_of_carousel = models.ManyToManyField(ImageOfCarouselOfEquipment)
    # images_of_abstract = models.ManyToManyField(ImageOfCarouselOfEquipment)

    def __str__(self):
        return '%s' % self.name


# class ImageOfAbstractOfEquipment(CommonFieldForAllModels):
#     uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
#     file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
#     format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
#     quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
#     title = models.CharField(max_length=100, blank=False, null=True)
#     equipment = models.ForeignKey(Equipment, blank=False, null=True, on_delete=models.CASCADE)
#     # class Meta:
#     #     abstract = True
#
#     def __str__(self):
#         return self.title
#
#     def image_tag(self):
#         return mark_safe('<img src="%s" />' % self.file.url)
#     image_tag.short_description = 'Image'


class VideoAboutEquipment(Video):
    objectAbout = models.ForeignKey(Equipment, blank=False, null=True, on_delete=models.CASCADE)
    # video = models.ManyToManyField(Video, blank=False)


class ImageAboutEquipment(Image):
    objectAbout = models.ForeignKey(Equipment, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image, blank=False)


class AbstractImageOfEquipment(Image):
    objectAbout = models.ForeignKey(Equipment, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image, blank=False)


class AudioAboutEquipment(Audio):
    objectAbout = models.ForeignKey(Equipment, blank=False, null=True, on_delete=models.CASCADE)
    # audio = models.ManyToManyField(Audio, blank=False)


class Vendor(CommonFieldForAllModels):
    representative = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True) # todo make these false.
    name = models.CharField(max_length=100, blank=False, null=False)
    aboutVendor = models.CharField(max_length=500, blank=False, null=False)
    is_factory = models.BooleanField(default=False)
    is_retailer = models.BooleanField(default=True)


class VideoAboutVendor(Video):
    objectAbout = models.ForeignKey(Vendor, blank=False, null=True, on_delete=models.CASCADE)
    # video = models.ManyToManyField(Video)


class ImageAboutVendor(Image):
    objectAbout = models.ForeignKey(Vendor, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image)


class AudioAboutVendor(Audio):
    objectAbout = models.ForeignKey(Vendor, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image)


class Address(CommonFieldForAllModels):
    address = models.CharField(max_length=300, blank=False, null=False)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, blank=False, null=False)


class Phone(CommonFieldForAllModels):
    phoneNumber = models.BigIntegerField(blank=False, null=False)
    vendor = models.ForeignKey(Vendor, blank=False, null=False, on_delete=models.CASCADE)


class SiteUrl(CommonFieldForAllModels):
    url = models.URLField(blank=False, null=False)
    vendor = models.ForeignKey(Vendor, blank=False, null=False, on_delete=models.CASCADE)


class Carpet(CommonFieldForAllModels):
    name = models.CharField(max_length=100, blank=False, null=False)
    province = models.CharField(max_length=100, blank=False, null=False) # todo add provinces.
    brand = models.CharField(max_length=100, blank=False, null=False)
    # metraj = todo
    shape = models.CharField(max_length=100, blank=False, null=False)  # todo add choices.
    backgroundColor = models.CharField(max_length=100, blank=False, null=False)  # todo add choices.
    # transverseDensity = models.
    # longitudinalDensity = models.
    khabYarn = models.CharField(max_length=100, blank=False, null=False)
    poodYarn = models.CharField(max_length=100, blank=False, null=False)

    retailStock = models.PositiveIntegerField(blank=True, null=True)  # todo
    retailDiscount = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    wholeSaleStock = models.PositiveIntegerField(blank=True, null=True)  # todo
    wholeSaleDiscount = models.DecimalField(max_digits=5, decimal_places=2)


class VideoAboutCarpet(Video):
    objectAbout = models.ForeignKey(Carpet, blank=False, null=True, on_delete=models.CASCADE)
    # video = models.ManyToManyField(Video, blank=False)


class ImageAboutCarpet(Image):
    objectAbout = models.ForeignKey(Carpet, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image, blank=False)


class AudioAboutCarpet(Audio):
    objectAbout = models.ForeignKey(Carpet, blank=False, null=True, on_delete=models.CASCADE)
    # audio = models.ManyToManyField(Audio, blank=False)


class SpectatorLikesEquipment(CommonFieldForAllModels):
    spectator = models.ForeignKey(Spectator, blank=False, null=False, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, blank=False, null=False, on_delete=models.CASCADE)
    like = models.BooleanField(blank=False, null=False)


class SpectatorViewsEquipment(CommonFieldForAllModels):
    spectator = models.ForeignKey(Spectator, blank=False, null=False, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, blank=False, null=False, on_delete=models.CASCADE)
    view = models.BooleanField(default=True)


class SpectatorCommentsEquipment(CommonFieldForAllModels):
    spectator = models.ForeignKey(Spectator, blank=False, null=False, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, blank=False, null=False, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500, blank=False, null=False)
    is_released = models.BooleanField(default=False)
    # todo add staff for confirm comments to release.