# from django.http import JsonResponse
from rest_framework.response import Response

# from creators.models import Game, Round
# from creators.serializers import GameSerializer, DeviceInstalledAppSerializer, RoundSerializer, \
#     PlayerFillsFormSerializer
from rest_framework import mixins
from rest_framework import generics

from django.contrib.auth import get_user_model

# from interactions.models import PlayerFillsForm
from preSelection.serializers import PreSelectionFormSerializer

import pandas as pd
import pickle

from preSelection.models import Equipment

DATA = pd.read_csv(r"preSelection\0&1.csv", sep=';')
DATA = DATA[:259]

test_df = DATA.head(2)  # adding equipment 1
test_df = test_df.append(DATA[10:13])  # adding equipment 2
test_df = test_df.append(DATA[23:26])  # adding equipment 3
test_df = test_df.append(DATA[38:41])  # adding equipment 4
test_df = test_df.append(DATA[52:57])  # adding equipment 5
test_df = test_df.append(DATA[73:76])  # adding equipment 6
test_df = test_df.append(DATA[88:93])  # adding equipment 7
test_df = test_df.append(DATA[112:115])  # adding equipment 8
test_df = test_df.append(DATA[126:128])  # adding equipment 9
test_df = test_df.append(DATA[140:141])  # adding equipment 10
test_df = test_df.append(DATA[145:146])  # adding equipment 11
test_df = test_df.append(DATA[150:158])  # adding equipment 12
test_df = test_df.append(DATA[187:193])  # adding equipment 13
test_df = test_df.append(DATA[217:219])  # adding equipment 14
test_df = test_df.append(DATA[233:236])  # adding equipment 15
test_df = test_df.append(DATA[251:253])  # adding equipment 16

train_df = DATA[2:10]  # adding equipment 1
train_df = train_df.append(DATA[13:23])  # adding equipment 2
train_df = train_df.append(DATA[26:38])  # adding equipment 3
train_df = train_df.append(DATA[41:52])  # adding equipment 4
train_df = train_df.append(DATA[57:73])  # adding equipment 5
train_df = train_df.append(DATA[76:88])  # adding equipment 6
train_df = train_df.append(DATA[93:112])  # adding equipment 7
train_df = train_df.append(DATA[115:126])  # adding equipment 8
train_df = train_df.append(DATA[128:140])  # adding equipment 9
train_df = train_df.append(DATA[141:145])  # adding equipment 10
train_df = train_df.append(DATA[146:150])  # adding equipment 11
train_df = train_df.append(DATA[158:187])  # adding equipment 12
train_df = train_df.append(DATA[193:217])  # adding equipment 13
train_df = train_df.append(DATA[219:233])  # adding equipment 14
train_df = train_df.append(DATA[236:251])  # adding equipment 15
train_df = train_df.append(DATA[253:259])  # adding equipment 16

train_x = train_df.drop('Equipment No.', 1)
train_y = train_df['Equipment No.']

# seperate the independent and target variable on testing data
test_x = test_df.drop('Equipment No.', 1)
test_y = test_df['Equipment No.']

import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import accuracy_score

from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification

model = RandomForestClassifier()
model.fit(train_x, train_y)

# predict the target on the train dataset
predict_train = model.predict(train_x)
print('\nTarget on train data', predict_train)

# Accuray Score on train dataset
accuracy_train = accuracy_score(train_y, predict_train)
print('\naccuracy_score on train dataset : ', accuracy_train)

# predict the target on the test dataset
predict_test = model.predict(test_x)
print('\nTarget on test data', predict_test)

# Accuracy Score on test dataset
accuracy_test = accuracy_score(test_y, predict_test)
print('\naccuracy_score on test dataset : ', accuracy_test)


ProjectUser = get_user_model()



class PreSelectionForm(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    # queryset = PlayerFillsForm.objects.all()   # todo this must retrieve all rounds of ... .(device_id or logined_user)
    serializer_class = PreSelectionFormSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    # def perform_create(self, serializer):
    #     # queryset = ProjectUser.objects.filter(user=self.request.user)
    #     # queryset = ProjectUser.objects.get(email=self.request.)
    #     # if queryset.exists():
    #     #     raise ValidationError('You have already signed up')
    #     # serializer.save(user=self.request.user)
    #     serializer.save()

    def post(self, request, *args, **kwargs):
        questionare_template = {
            'Minimum initial cost': [0],
            'Minimum Annual O&M cost': [0],
            'Minimum annual energy cost (bills)': [0],
            'Maximum lifespan': [0],
            'Speed  of respond to load': [0],
            'Cooling and heating simultaneously': [0],
            'Environment-friendly components': [0],
            'Minimum CO2 emission': [0],
            'Best rate of Eco-labeling(Green Building norms)': [0],
            'Range of cool load 3-20 kW': [0],
            'Range of cool load 20-50 kW': [0],
            'Range of cool load 50-200 kW': [0],
            'Range of cool load 200-1000 kW': [0],
            'Range of cool load 1000-5000 kW': [0],
            'Temperatur level [°C]=>6-12°C=1=>10-16°C =0': [0],
            'Is the equipment flexible about type of fan-coil': [0],
            'The equipment does not need any large space': [0],
            'More than 80%': [0],
            'Water': [0],
            'Gas': [0],
            'Electricity': [0]
        }

        questionare = request.data['model']
        for k, v in questionare.items():
            if v == False:
                questionare[k] = '1'
            elif v == True:
                questionare[k] = '5'
            else:
                questionare[k] = v

        for k, v in questionare_template.items():
            try:
                questionare_template[k] = [0] if int(questionare[k]) <= 3 else [1]
            except:
                print(k)

        # save the model to disk
        filename = 'finalized_DT_model.sav'
        pickle.dump(model, open(filename, 'wb'))

        # X1_test = pd.read_csv(r"preSelection\1)Bitte geben Sie die Anforderungen an.csv", sep=';')
        X1_test = pd.DataFrame.from_dict(questionare_template)


        # load the model from disk
        loaded_model = pickle.load(open(filename, 'rb'))
        result = loaded_model.predict(X1_test)
        print(result)

        if result == [1]:
            a = "Absorption Chiller Single Effect"

        elif result == [2]:
            a = "Absorption Chiller Double Effect"

        elif result == [3]:
            a = "Absorption  Chiller Direct Fire"

        elif result == [4]:
            a = "Water cooled centrifugal compressor chiller"

        elif result == [5]:
            a = "Air cooled Centrifugal compressor chiller"

        elif result == [6]:
            a = "Water cooled screw compressor chiller"

        elif result == [7]:
            a = "Air cooled screw compressor chiller"

        elif result == [8]:
            a = "Water cooled scroll compressor chiller"

        elif result == [9]:
            a = "Air cooled scroll compressor chiller"

        elif result == [10]:
            a = "Water cooled reciprocating compressor chiller"

        elif result == [11]:
            a = "Air cooled Reciprocating compressor chiller"

        elif result == [12]:
            a = "Heat Pump - Water"

        elif result == [13]:
            a = "Heat Pump - Geothermal"

        elif result == [14]:
            a = "AC Split"

        elif result == [15]:
            a = "VRF or VRV"

        elif result == [16]:
            a = "Evaporative cooler (Verdunstungskühlung)"

        equipment_result = Equipment.objects.get(name=a)
        response_dic = {
            'media': {
                'alt': equipment_result.name,
                'height': "140",
                'imgUrl': 'http://localhost:8000' + equipment_result.abstractimageofequipment_set.all()[0].file.url,
                'title': equipment_result.name
            },
            'title': equipment_result.name,
            'body': 'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica'
        }

        # game_id = kwargs['game_id']
        # game = Game.objects.get_or_create(id=game_id, device_id="52ab6165-c0f6-4163-aa25-c223d55a00d6")
        # play_saved = self.create(request, *args, **kwargs)
        # return Response(game_created, status=201)
        # return play_saved
        # print('hhello')
        return Response({'recommended device': response_dic}, status=201)
        # return Response(request.data['model'], status=201)
