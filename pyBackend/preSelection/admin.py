# # todo/admin.py

from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from django.forms import forms
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import AdminFileWidget
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model


from preSelection.models import Category, Equipment, AbstractImageOfEquipment, ImageAboutEquipment
ProjectUser = get_user_model()


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        output = []

        if value and getattr(value, "url", None):
            image_url = value.url
            file_name = str(value)

            output.append(
                f' <a href="{image_url}" target="_blank">'
                f'  <img src="{image_url}" alt="{file_name}" width="150" height="150" '
                f'style="object-fit: cover;"/> </a>')

        output.append(super(AdminFileWidget, self).render(name, value, attrs, renderer))
        return mark_safe(u''.join(output))


# class EquipmentImageInline(admin.StackedInline):
#     model = Equipment.images_of_carousel.through
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class AbstractImageOfEquipmentInline(admin.StackedInline):
    model = AbstractImageOfEquipment
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


class ImageAboutEquipmentInline(admin.StackedInline):
    model = ImageAboutEquipment
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True
    show_full_result_count = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


# class ImageAboutEquipmentInline(admin.StackedInline):
#     model = ImageAboutEquipment
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class ImageOfCarouselOfEquipmentAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         EquipmentImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class AbstractImageOfEquipmentAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         # EquipmentImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class EquipmentAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            # 'fields': ('header', 'body', 'author', 'content', 'category', 'images_of_carousel')
            'fields': (('name', 'is_active',), 'author', 'category', 'articleAboutEquipment', 'description')
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )
    filter_horizontal = ('category',)
    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }

    inlines = [
        # EquipmentImageInline, EquipmentAbstractImageInline, ImageAboutEquipmentInline
        ImageAboutEquipmentInline, AbstractImageOfEquipmentInline,
    ]
    # exclude = ('images_of_carousel',)


admin.site.register(Equipment, EquipmentAdmin)
# admin.site.register(ImageOfCarouselOfEquipment, ImageOfCarouselOfEquipmentAdmin)




# @admin.register(Equipment)
# class EquipmentPageAdmin(admin.ModelAdmin):
#     fieldsets = (
#         (None, {
#             'fields': ('header', 'body', 'author', 'category')
#         }),
#         # ('Image Of Cards', {
#         #     'classes': ('collapse', 'extrapretty'),
#         #     'fields': ('registration_required', 'template_name'),
#         #     'description':'some description'
#         # }),
#     )
#     # filter_horizontal = (
#     #     'imageaboutequipment',
#     # )
#     inlines = [
#         'CollectionImageInline',
#     ]

    # class EquipmentModelForm(forms.ModelForm):
    #     class Meta:
    #         model = ImageAboutEquipment
    #
    #     def __init__(self, *args, **kwargs):
    #         forms.ModelForm.__init__(self, *args, **kwargs)
    #         self.fields['imageaboutequipment'].queryset = ImageAboutEquipment.avail.all()


admin.site.register(Category)
# admin.site.register(Equipment, EquipmentPageAdmin)
# admin.site.register(Equipment, EquipmentPageAdmin)
# admin.site.register(AbstractImageOfEquipment, AbstractImageOfEquipmentAdmin)
# admin.site.register(ImageAboutEquipment)
# admin.site.register(ImageOfCarouselsOfEquipment)
admin.site.register(ProjectUser)
