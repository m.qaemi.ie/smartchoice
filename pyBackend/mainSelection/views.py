# from django.http import JsonResponse
from rest_framework.response import Response

# from creators.models import Game, Round
# from creators.serializers import GameSerializer, DeviceInstalledAppSerializer, RoundSerializer, \
#     PlayerFillsFormSerializer
from rest_framework import mixins
from rest_framework import generics

from django.contrib.auth import get_user_model

# from interactions.models import PlayerFillsForm
from mainSelection.serializers import MainSelectionFormSerializer

import sqlite3
import pandas as pd
from sqlalchemy import create_engine

file = 'mainSelection/MainSelectionStepDataset.xlsx'
# output = 'output.xlsx'

engine = create_engine('sqlite://', echo=False)
df = pd.read_excel(file, sheet_name='MainSelectionDataset')
# df.to_sql('cooling', engine, if_exists='replace', index=False)

# from preSelection.models import Equipment


ProjectUser = get_user_model()

class MainSelectionForm(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    # queryset = PlayerFillsForm.objects.all()   # todo this must retrieve all rounds of ... .(device_id or logined_user)
    serializer_class = MainSelectionFormSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    # def perform_create(self, serializer):
    #     # queryset = ProjectUser.objects.filter(user=self.request.user)
    #     # queryset = ProjectUser.objects.get(email=self.request.)
    #     # if queryset.exists():
    #     #     raise ValidationError('You have already signed up')
    #     # serializer.save(user=self.request.user)
    #     serializer.save()

    def post(self, request, *args, **kwargs):
        questionare_template = {
            'CoolCapacityKWLowerBound': '',
            'CoolCapacityKWUpperBound': '',
            'ChilledWaterOutletTemperaturCLowerBound': '',
            'ChilledWaterOutletTemperaturCUpperBound': '',
            'CoolingWaterOutletTemperaturCLowerBound': '',
            'CoolingWaterOutletTemperaturCUpperBound': '',
            'PriceRangeLowerBound': '',
            'PriceRangeUpperBound': '',
            'Manufacturer': '',
            # 'Carrier': '',
            # 'York': '',
            # 'Daikin': '',
        }

        questionare = request.data['model']

        EquipmentType = questionare.pop('equipmentType')
        # EquipmentType = 'Absorption Chiller Single Effect'

        Manufacturers = ['Carrier', 'York', 'Daikin']

        for k, v in questionare.items():  # todo change this to accept multi manufacturer.
            if k in Manufacturers:
                questionare_template['Manufacturer'] = k
            else:
                questionare_template[k] = v

        # for k in list(questionare_template):
        #     if not questionare_template[k]:
        #         del questionare_template[k]

        query_to_database = "Select * from cooling where " + f"EquipmentType='{EquipmentType}'"
        query_to_database += ' AND ' + ' Manufacturer' + '=' + f"'{questionare_template['Manufacturer']}'"
        for k, v in questionare_template.items():
            if not v:
                continue
            # elif k is 'Manufacturer':
            #     query_to_database += ' Manufacturer' + '=' + v
            #     continue
            elif 'UpperBound' in k:
                query_to_database += ' AND ' + k.replace('UpperBound', '') + '<' + v
                continue
            elif 'LowerBound' in k:
                query_to_database += ' AND ' + k.replace('LowerBound', '') + '>' + v
                continue

        # EquipmentType = 'Absorption Chiller Single Effect'
        # Manufacturer = 'Carrier'
        # CoolCapacityKWUpperBound = 150
        # ChilledWaterOutletTemperaturCUpperBound = 8
        # CoolingWaterOutletTemperaturCUpperBound = 32
        # query_to_database = f"Select * from cooling where \
        # EquipmentType='{EquipmentType}' \
        # AND Manufacturer='{Manufacturer}' \
        # AND CoolCapacityKW<{CoolCapacityKWUpperBound} \
        # AND ChilledWaterOutletTemperaturC<{ChilledWaterOutletTemperaturCUpperBound} \
        # AND CoolingWaterOutletTemperaturC<{CoolingWaterOutletTemperaturCUpperBound} "

        df.to_sql('cooling', engine, if_exists='replace', index=False)
        results = engine.execute(query_to_database)

        final = pd.DataFrame(results, columns=df.columns)
        response_dic = final.to_dict('index')
        response_list = [v for v in response_dic.values()]
        # final.to_excel(output, index=False)

        # equipment_result = Equipment.objects.get(name=a)
        # response_dic = {
        #     'media': {
        #         'alt': equipment_result.name,
        #         'height': "140",
        #         'imgUrl': 'http://localhost:8000' + equipment_result.abstractimageofequipment_set.all()[0].file.url,
        #         'title': equipment_result.name
        #     },
        #     'title': equipment_result.name,
        #     'body': 'Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica'
        # }

        # game_id = kwargs['game_id']
        # game = Game.objects.get_or_create(id=game_id, device_id="52ab6165-c0f6-4163-aa25-c223d55a00d6")
        # play_saved = self.create(request, *args, **kwargs)
        # return Response(game_created, status=201)
        # return play_saved
        # print('hhello')
        # return Response({'recommended device': response_dic}, status=201)
        return Response({'recommendedProductNo': response_list}, status=201)
        # return Response(request.data['model'], status=201)
