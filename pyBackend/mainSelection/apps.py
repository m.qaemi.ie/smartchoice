from django.apps import AppConfig


class PreselectionConfig(AppConfig):
    name = 'mainSelection'
