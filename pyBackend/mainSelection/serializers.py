from django.db.models import Q
from django.db import IntegrityError
from rest_framework import serializers
# from creators.models import Game

from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError
# from preSelection.models import Equipment, VideoAboutEquipment, Vendor, Carpet, AbstractImageOfEquipment, Category, ImageAboutEquipment

# from creators.models import Game, Round, Player, Referee, FormTemplate
# from interactions.models import DeviceInstalledApp, PlayerFillsForm

ProjectUser = get_user_model()


class MainSelectionFormSerializer(serializers.Serializer):
    # email = serializers.EmailField()
    # email = serializers.EmailField()
    pass

#
# class AbstractImageUploadSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = AbstractImageOfEquipment
#         fields = '__all__'
#
#
# class ImageAboutEquipmentSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ImageAboutEquipment
#         fields = '__all__'
#
#
# # fs = [f.name for f in Equipment._meta.get_fields()]
#
#
# class CategoryListSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = '__all__'
#
#     def create(self, validated_data):
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         obj = Category.objects.create(**validated_data)
#         return obj
#
#
# # class CategoryAssignToEquipmentSerializer(serializers.ModelSerializer):
# #     class Meta:
# #         model = Category
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         # user = []
# #         # device_id = validated_data['device_id']
# #         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
# #         obj = Category.objects.create(**validated_data)
# #         return obj
#
#
# class CategoryAssignToEquipmentSerializer(serializers.ModelSerializer):
#     # title = serializers.CharField()
#
#     class Meta:
#         model = Category
#         fields = ['title']
#
#     def create(self, validated_data):
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         obj = Category.objects.create(**validated_data)
#         return obj
#
#
# # class CategoryAssignToEquipmentSerializer(serializers.RelatedField):
# #     def to_internal_value(self, data):
# #
# #     def to_representation(self, value):
# #         # duration = time.strftime('%M:%S', time.gmtime(value.duration))
# #         # return 'Track %d: %s (%s)' % (value.order, value.name, duration)
# #         return '%s' % (value.title)
#
# class EquipmentListSerializer(serializers.ModelSerializer):
#     abstractimageofequipment_set = AbstractImageUploadSerializer(many=True, read_only=True)
#     imageaboutequipment_set = ImageAboutEquipmentSerializer(many=True, read_only=True)
#     # category = CategoryAssignToEquipmentSerializer(many=True)
#     # category = serializers.SlugRelatedField(many=True, read_only=True, slug_field="title")
#     category = serializers.StringRelatedField(many=True)
#
#     class Meta:
#         model = Equipment
#         fields = ['id', 'body', 'name', 'abstractimageofequipment_set', 'category', 'imageaboutequipment_set', 'articleAboutEquipment']
#         # fields = [f.name for f in Equipment._meta.get_fields()]
#         # fields = fs.append("abstractimageofequipment_set")
#         # fields = '__all__'
#
#     def create(self, validated_data):
#         categories = validated_data.pop('category')
#         category_set = []
#
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         obj = Equipment.objects.create(**validated_data)
#         for category in categories:
#
#             category = Category.objects.get(title=category['title'])
#             category_set.append(category)
#         obj.category.set(category_set)
#         return obj
#
#     def update(self, instance, validated_data):
#         categories = validated_data.pop('category')
#         category_set = []
#
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         # obj = Equipment.objects.get(**validated_data)
#         for category in categories:
#
#             category = Category.objects.get(title=category['title'])
#             category_set.append(category)
#         instance.category.set(category_set)
#         return instance
#
#
#
# class VendorListSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Vendor
#         fields = '__all__'
#
#     def create(self, validated_data):
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         obj = Vendor.objects.create(**validated_data)
#         return obj
#
#
# class CarpetListSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Carpet
#         fields = '__all__'
#
#     def create(self, validated_data):
#         # user = []
#         # device_id = validated_data['device_id']
#         # obj = DeviceInstalledApp.objects.create(device_id=device_id)
#         obj = Carpet.objects.create(**validated_data)
#         return obj
#
#
# class VideoUploadSerializer(serializers.Serializer):
#     file = serializers.FileField()
#     # title = serializers.CharField()
#
#
#     def create(self, validated_data):
#         fileAboutObjectModel = validated_data.pop('fileAboutObjectModel')
#
#     # def save(self, **validated_data):
#         file = fileAboutObjectModel.objects.create(**validated_data)  # todo send to background , video=fileLocation
#
#         # video_added = equipment.videoaboutequipment_set.add(video=fileLocation)
#         # equipmentVideo.save()  # todo send to background
#         return file
#
#
# class AudioUploadSerializer(serializers.Serializer):
#     file = serializers.FileField()
#     # title = serializers.CharField()
#
#     def create(self, validated_data):
#         fileAboutObjectModel = validated_data.pop('fileAboutObjectModel')
#
#     # def save(self, **validated_data):
#         file = fileAboutObjectModel.objects.create(**validated_data)  # todo send to background , video=fileLocation
#
#         # video_added = equipment.videoaboutequipment_set.add(video=fileLocation)
#         # equipmentVideo.save()  # todo send to background
#         return file
#
#
# class ImageUploadSerializer(serializers.Serializer):
#     file = serializers.ImageField()
#
#     def create(self, validated_data):
#         fileAboutObjectModel = validated_data.pop('fileAboutObjectModel')
#         file = fileAboutObjectModel.objects.create(**validated_data)  # todo send to background , video=fileLocation
#         return file
#
#
# # '''
# # class DeviceSerializer(serializers.ModelSerializer):
# #     # device_id = serializers.CharField(required=True)
# #     class Meta:
# #         model = ProjectUser
# #         fields = ['devices']
# #
# #     def create(self, validated_data):
# #         # device_id = validated_data['devices'][0]
# #         device_id = validated_data['device_id']
# #         validated_data['email'] = 'device_user_{}@mail.com'.format(device_id)
# #         # validated_data['devices'] = {"device_ids": [validated_data['devices']]}
# #         # try:
# #             # device = Device.objects.get(device_id=device_id, is_active=True)
# #             # device_user = ProjectUser.device_set.get(device_id=device)  # improve this. how many times does db hit?
# #             # return device_user
# #         # except (Device.DoesNotExist, ProjectUser.DoesNotExist):
# #         #     device_user = ProjectUser.objects.create(**validated_data)
# #             # device = Device.objects.create(device_id=device_id, user=device_user)
# #             # return device_user
# #         # try:
# #         #     device_user = ProjectUser.objects.get(Device=device_id)
# #         # except IntegrityError:
# #         #     raise ValidationError()
# #         # try:
# #         #     device_user = ProjectUser.objects.create_device_user(**validated_data)
# #         # except IntegrityError:
# #         #     raise ValidationError()
# #         # return device_user
# # '''
# #
# #
# # class PlayerSerializer(serializers.ModelSerializer):
# #     class Meta:
# #         model = Player
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         player = Player.objects.get_or_create(**validated_data)
# #         return player[0]
# #
# #
# # class RoundSerializer(serializers.ModelSerializer):
# #
# #     class Meta:
# #         model = Round
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         # device_id = validated_data['device_id']
# #         round = Round.objects.get_or_create(**validated_data)
# #         return round[0]
# #
# #
# #     # def create(self, validated_data):  # todo wrap in a transaction.
# #     #     player_validated_data = validated_data.pop('players')
# #     #     round = Round.objects.create(**validated_data)
# #     #     players_serializer = self.fields['players']
# #     #     # for each in player_validated_data:
# #     #     #     each['round'] = round
# #     #     players = players_serializer.create(player_validated_data)
# #     #     return round
# #
# #
# # class GameSerializer(serializers.ModelSerializer):
# #     round_set = RoundSerializer(many=True)
# #
# #     class Meta:
# #         model = Game
# #         fields = ['device_id', 'gameEnvironment', 'creator', 'typeOfReferee', 'round_set']
# #
# #     def create(self, validated_data):  # todo wrap in a transaction.
# #         round_validated_data = validated_data.pop('round_set')
# #         game = Game.objects.create(**validated_data)
# #         round_set_serializer = self.fields['round_set']
# #         for each in round_validated_data:
# #             each['game'] = game
# #         rounds = round_set_serializer.create(round_validated_data)
# #         return game
# #
# #
# # class FormTemplateSerializer(serializers.ModelSerializer):
# #     class Meta:
# #         model = FormTemplate
# #         fields = '__all__'
# #
# #     # def create(self, validated_data):
# #     #     formTemplate = FormTemplate.objects.get_or_create(**validated_data)
# #     #     return form
# #
# #
# # class RefereeSerializer(serializers.ModelSerializer):
# #     class Meta:
# #         model = Referee
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         referee = Referee.objects.get_or_create(**validated_data)
# #         return referee[0]
# #
# #
# # class PlayerFillsFormSerializer(serializers.ModelSerializer):
# #     round = RoundSerializer()
# #     player = PlayerSerializer()
# #     formTemplate = FormTemplateSerializer(required=False)
# #     referee = RefereeSerializer()
# #
# #     class Meta:
# #         model = PlayerFillsForm
# #         # fields = '__all__'
# #         fields = ['round', 'player', 'formTemplate', 'form', 'referee']
# #
# #          # = models.ForeignKey(Round, blank=True, null=True, on_delete=models.DO_NOTHING)
# #          # = models.ForeignKey(Player, blank=False, null=False, on_delete=models.DO_NOTHING)
# #          # = models.ForeignKey(FormFields, blank=True, null=True, on_delete=models.DO_NOTHING)
# #          # = JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
# #          # = models.ForeignKey(Referee, blank=False, null=False, on_delete=models.DO_NOTHING)
# #
# #     def create(self, validated_data):  # todo wrap in a transaction.
# #         round_validated_data = validated_data.pop('round')
# #         round_serializer = self.fields['round']
# #         round = round_serializer.create(round_validated_data)
# #
# #         player_validated_data = validated_data.pop('player')
# #         player_serializer = self.fields['player']
# #         player = player_serializer.create(player_validated_data)
# #
# #         # referee_validated_data = validated_data.pop('referee')
# #         # referee_serializer = self.fields['referee']
# #         # referee = referee_serializer.create(referee_validated_data)
# #
# #         # formTemplate_validated_data = validated_data.pop('formTemplate') todo
# #
# #         playerFillsForm = PlayerFillsForm.objects.create(device_id=player.device_id, round=round, player=player)
# #         # for each in round_validated_data:
# #         #     each['game'] = game
# #         return playerFillsForm
# #
# #         # round_validated_data = validated_data.pop('round_set')
# #         # game = Game.objects.create(**validated_data)
# #         # round_set_serializer = self.fields['round_set']
# #         # for each in round_validated_data:
# #         #     each['game'] = game
# #         # rounds = round_set_serializer.create(round_validated_data)
# #         # return game
# #
# #         # # device_id = validated_data['device_id']
# #         # game_created = Game.objects.create(**validated_data)
# #         # return game_created
# #
# #
# # '''
# # class AnonymousUserSerializer(serializers.ModelSerializer):
# #
# #     class Meta:
# #         model = ProjectUser
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         return ProjectUser.objects.create(**validated_data)
# #
# #
# # class GameSerializer(serializers.ModelSerializer):
# #
# #     class Meta:
# #         model = Game
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         return Game.objects.create(**validated_data)
# #
# # '''
# # '''
# #
# # class ProviderCreatorSerializer(serializers.ModelSerializer):
# #
# #     class Meta:
# #         model = ProjectUser
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         # todo send sms to mobile. if match continue. needs async with celery.
# #         return ProjectUser.objects.create_provider_creator(**validated_data)
# #
# #
# # class ProviderSerializer(serializers.ModelSerializer):
# #
# #     class Meta:
# #         model = Service
# #         fields = '__all__'
# #
# #     def create(self, validated_data):
# #         provider = Provider(name=validated_data['name'])
# #         provider.save()
# #         validated_data['provider'] = provider
# #         return Service.objects.create(**validated_data)
# #
# # '''