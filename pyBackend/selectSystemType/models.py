from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from creators.models import CommonFieldForAllModels
from django.contrib.postgres.fields import JSONField, ArrayField
from django.core.serializers.json import DjangoJSONEncoder

from django.contrib.auth import get_user_model

import uuid

from django.utils.safestring import mark_safe

# from tinymce.models import HTMLField
# from ckeditor.fields import RichTextField


# class Spectator(CommonFieldForAllModels):  # Spectator can be ProjectUser or Device only.
#     projectUser = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.DO_NOTHING)


class Video(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.FileField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True


class Image(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class ImageOfCarouselOfSystemType(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=False, null=True)
    # class Meta:
    #     abstract = True

    def __str__(self):
        return self.title

    def image_tag(self):
        return mark_safe('<img src="%s" />' % self.file.url)
    image_tag.short_description = 'Image'


class Audio(CommonFieldForAllModels):
    uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
    file = models.FileField(blank=True, null=True)  # todo blank = False, null = False
    format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
    quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
    title = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True


class SystemTypeCategory(CommonFieldForAllModels):
    title = models.CharField(max_length=100, blank=False, null=False, unique=True)

    def __str__(self):
        return '%s' % self.title


class SystemType(CommonFieldForAllModels):
    author = models.ForeignKey(get_user_model(), related_name='author_systemtype', on_delete=models.DO_NOTHING, blank=True, null=True)
    # video = models.ManyToManyField(Video)
    # image = models.ManyToManyField(Image)
    # audio = models.ManyToManyField(Audio)
    name = models.CharField(max_length=100, blank=False, null=False)
    about = models.CharField(max_length=1000, blank=False, null=False)
    # content = HTMLField(blank=True, null=True)
    articleAboutSystemType = RichTextUploadingField(blank=True, null=True)
    # userLikesSystemType = models.ManyToManyField(get_user_model(), related_name='userLikeSystemType')
    systemtypecategory = models.ManyToManyField(SystemTypeCategory)
    # images_of_carousel = models.ManyToManyField(ImageOfCarouselOfSystemType)
    # images_of_abstract = models.ManyToManyField(ImageOfCarouselOfSystemType)

    def __str__(self):
        return '%s' % self.name


# class ImageOfAbstractOfSystemType(CommonFieldForAllModels):
#     uploader = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, blank=True, null=True)  # todo make these false.
#     file = models.ImageField(blank=True, null=True)  # todo blank = False, null = False
#     format = models.CharField(max_length=100, blank=True, null=True)  # todo add choices and meme/type
#     quality = models.CharField(max_length=100, blank=True, null=True)  # todo add choices
#     title = models.CharField(max_length=100, blank=False, null=True)
#     systemtype = models.ForeignKey(SystemType, blank=False, null=True, on_delete=models.CASCADE)
#     # class Meta:
#     #     abstract = True
#
#     def __str__(self):
#         return self.title
#
#     def image_tag(self):
#         return mark_safe('<img src="%s" />' % self.file.url)
#     image_tag.short_description = 'Image'


class VideoAboutSystemType(Video):
    objectAbout = models.ForeignKey(SystemType, blank=False, null=True, on_delete=models.CASCADE)
    # video = models.ManyToManyField(Video, blank=False)


class ImageAboutSystemType(Image):
    objectAbout = models.ForeignKey(SystemType, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image, blank=False)


class AbstractImageOfSystemType(Image):
    objectAbout = models.ForeignKey(SystemType, blank=False, null=True, on_delete=models.CASCADE)
    # image = models.ManyToManyField(Image, blank=False)


class AudioAboutSystemType(Audio):
    objectAbout = models.ForeignKey(SystemType, blank=False, null=True, on_delete=models.CASCADE)
    # audio = models.ManyToManyField(Audio, blank=False)
