# # todo/admin.py

from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from django.forms import forms
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import AdminFileWidget
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model


from selectSystemType.models import SystemTypeCategory, SystemType, AbstractImageOfSystemType, ImageAboutSystemType
ProjectUser = get_user_model()


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        output = []

        if value and getattr(value, "url", None):
            image_url = value.url
            file_name = str(value)

            output.append(
                f' <a href="{image_url}" target="_blank">'
                f'  <img src="{image_url}" alt="{file_name}" width="150" height="150" '
                f'style="object-fit: cover;"/> </a>')

        output.append(super(AdminFileWidget, self).render(name, value, attrs, renderer))
        return mark_safe(u''.join(output))


# class SystemTypeImageInline(admin.StackedInline):
#     model = SystemType.images_of_carousel.through
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class AbstractImageOfSystemTypeInline(admin.StackedInline):
    model = AbstractImageOfSystemType
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


class ImageAboutSystemTypeInline(admin.StackedInline):
    model = ImageAboutSystemType
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True
    show_full_result_count = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


# class ImageAboutSystemTypeInline(admin.StackedInline):
#     model = ImageAboutSystemType
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class ImageOfCarouselOfSystemTypeAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         SystemTypeImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class AbstractImageOfSystemTypeAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         # SystemTypeImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class SystemTypeAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            # 'fields': ('header', 'body', 'author', 'content', 'locationtypecategory', 'images_of_carousel')
            'fields': (('name', 'is_active',), 'author', 'systemtypecategory', 'articleAboutSystemType', 'description')
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )
    filter_horizontal = ('systemtypecategory',)
    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }

    inlines = [
        # SystemTypeImageInline, SystemTypeAbstractImageInline, ImageAboutSystemTypeInline
        ImageAboutSystemTypeInline, AbstractImageOfSystemTypeInline,
    ]
    # exclude = ('images_of_carousel',)


admin.site.register(SystemType, SystemTypeAdmin)
# admin.site.register(ImageOfCarouselOfSystemType, ImageOfCarouselOfSystemTypeAdmin)




# @admin.register(SystemType)
# class SystemTypePageAdmin(admin.ModelAdmin):
#     fieldsets = (
#         (None, {
#             'fields': ('header', 'body', 'author', 'systemtypecategory')
#         }),
#         # ('Image Of Cards', {
#         #     'classes': ('collapse', 'extrapretty'),
#         #     'fields': ('registration_required', 'template_name'),
#         #     'description':'some description'
#         # }),
#     )
#     # filter_horizontal = (
#     #     'imageaboutsystemtype',
#     # )
#     inlines = [
#         'CollectionImageInline',
#     ]

    # class SystemTypeModelForm(forms.ModelForm):
    #     class Meta:
    #         model = ImageAboutSystemType
    #
    #     def __init__(self, *args, **kwargs):
    #         forms.ModelForm.__init__(self, *args, **kwargs)
    #         self.fields['imageaboutsystemtype'].queryset = ImageAboutSystemType.avail.all()


admin.site.register(SystemTypeCategory)
# admin.site.register(SystemType, SystemTypePageAdmin)
# admin.site.register(SystemType, SystemTypePageAdmin)
# admin.site.register(AbstractImageOfSystemType, AbstractImageOfSystemTypeAdmin)
# admin.site.register(ImageAboutSystemType)
# admin.site.register(ImageOfCarouselsOfSystemType)
# admin.site.register(ProjectUser)
