from django.apps import AppConfig


class PreselectionConfig(AppConfig):
    name = 'selectLocationType'
