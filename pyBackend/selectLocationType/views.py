# from django.http import JsonResponse
from rest_framework.response import Response

# from creators.models import Game, Round
# from creators.serializers import GameSerializer, DeviceInstalledAppSerializer, RoundSerializer, \
#     PlayerFillsFormSerializer
from rest_framework import mixins
from rest_framework import generics

from django.contrib.auth import get_user_model

# from interactions.models import PlayerFillsForm
from selectLocationType.serializers import LocationTypeListSerializer
from selectLocationType.models import LocationType

import pandas as pd
import pickle

# from preSelection.models import Equipment


ProjectUser = get_user_model()


class LocationType(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = LocationType.objects.all()   # todo this must retrieve all rounds of ... .(device_id or logined_user)
    serializer_class = LocationTypeListSerializer
    # parser_classes = [JSONParser]

    def get(self, request, *args, **kwargs):  # todo
        return self.list(request, *args, **kwargs)

    # def perform_create(self, serializer):
    #     # queryset = ProjectUser.objects.filter(user=self.request.user)
    #     # queryset = ProjectUser.objects.get(email=self.request.)
    #     # if queryset.exists():
    #     #     raise ValidationError('You have already signed up')
    #     # serializer.save(user=self.request.user)
    #     serializer.save()

    # def post(self, request, *args, **kwargs):
    #     equipment_result = Equipment.objects.get(name=a)
    #
    #     # game_id = kwargs['game_id']
    #     # game = Game.objects.get_or_create(id=game_id, device_id="52ab6165-c0f6-4163-aa25-c223d55a00d6")
    #     # play_saved = self.create(request, *args, **kwargs)
    #     # return Response(game_created, status=201)
    #     # return play_saved
    #     # print('hhello')
    #     return Response({'recommended device': response_dic}, status=201)
    #     # return Response(request.data['model'], status=201)
