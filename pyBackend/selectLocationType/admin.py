# # todo/admin.py

from django.contrib import admin
from django.contrib.admin.options import InlineModelAdmin
from django.forms import forms
from django.utils.safestring import mark_safe
from django.contrib.admin.widgets import AdminFileWidget
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model


from selectLocationType.models import LocationTypeCategory, LocationType, AbstractImageOfLocationType, ImageAboutLocationType
ProjectUser = get_user_model()


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        output = []

        if value and getattr(value, "url", None):
            image_url = value.url
            file_name = str(value)

            output.append(
                f' <a href="{image_url}" target="_blank">'
                f'  <img src="{image_url}" alt="{file_name}" width="150" height="150" '
                f'style="object-fit: cover;"/> </a>')

        output.append(super(AdminFileWidget, self).render(name, value, attrs, renderer))
        return mark_safe(u''.join(output))


# class LocationTypeImageInline(admin.StackedInline):
#     model = LocationType.images_of_carousel.through
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class AbstractImageOfLocationTypeInline(admin.StackedInline):
    model = AbstractImageOfLocationType
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


class ImageAboutLocationTypeInline(admin.StackedInline):
    model = ImageAboutLocationType
    # readonly_fields = ['image_tag', ]
    classes = ('collapse', 'extrapretty')
    extra = 1
    can_delete = True
    show_change_link = True
    show_full_result_count = True

    fieldsets = (
        (None, {
            'fields': (('title', 'is_active', 'file',), ('uploader',), ('format', 'quality', 'description',)),
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )

    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }


# class ImageAboutLocationTypeInline(admin.StackedInline):
#     model = ImageAboutLocationType
#     # readonly_fields = ['image_tag', ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class ImageOfCarouselOfLocationTypeAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         LocationTypeImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


# class AbstractImageOfLocationTypeAdmin(admin.ModelAdmin):
#     # fields = ('image_tag',)
#     # readonly_fields = ['image_tag',]
#     inlines = [
#         # LocationTypeImageInline,
#     ]
#     formfield_overrides = {
#         models.ImageField: {
#             'widget': AdminImageWidget
#         },
#     }


class LocationTypeAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            # 'fields': ('header', 'body', 'author', 'content', 'locationtypecategory', 'images_of_carousel')
            'fields': (('name', 'is_active',), 'author', 'locationtypecategory', 'articleAboutLocationType', 'description')
        }),
        # ('Image Of Cards', {
        #     'classes': ('collapse', 'extrapretty'),
        #     'fields': ('registration_required', 'template_name'),
        #     'description':'some description'
        # }),
    )
    filter_horizontal = ('locationtypecategory',)
    formfield_overrides = {
        models.ImageField: {
            'widget': AdminImageWidget
        },
    }

    inlines = [
        # LocationTypeImageInline, LocationTypeAbstractImageInline, ImageAboutLocationTypeInline
        ImageAboutLocationTypeInline, AbstractImageOfLocationTypeInline,
    ]
    # exclude = ('images_of_carousel',)


admin.site.register(LocationType, LocationTypeAdmin)
# admin.site.register(ImageOfCarouselOfLocationType, ImageOfCarouselOfLocationTypeAdmin)




# @admin.register(LocationType)
# class LocationTypePageAdmin(admin.ModelAdmin):
#     fieldsets = (
#         (None, {
#             'fields': ('header', 'body', 'author', 'locationtypecategory')
#         }),
#         # ('Image Of Cards', {
#         #     'classes': ('collapse', 'extrapretty'),
#         #     'fields': ('registration_required', 'template_name'),
#         #     'description':'some description'
#         # }),
#     )
#     # filter_horizontal = (
#     #     'imageaboutlocationtype',
#     # )
#     inlines = [
#         'CollectionImageInline',
#     ]

    # class LocationTypeModelForm(forms.ModelForm):
    #     class Meta:
    #         model = ImageAboutLocationType
    #
    #     def __init__(self, *args, **kwargs):
    #         forms.ModelForm.__init__(self, *args, **kwargs)
    #         self.fields['imageaboutlocationtype'].queryset = ImageAboutLocationType.avail.all()


admin.site.register(LocationTypeCategory)
# admin.site.register(LocationType, LocationTypePageAdmin)
# admin.site.register(LocationType, LocationTypePageAdmin)
# admin.site.register(AbstractImageOfLocationType, AbstractImageOfLocationTypeAdmin)
# admin.site.register(ImageAboutLocationType)
# admin.site.register(ImageOfCarouselsOfLocationType)
# admin.site.register(ProjectUser)
